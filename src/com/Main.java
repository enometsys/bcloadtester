package com;

import java.util.Random;

import com.connection.TCPConnectionHandler;
import com.packet.Packet;
import com.utils.Logger;

public final class Main{
	public static final boolean 		test = Packet.test;
	public static final boolean 		debug = true;
	public static boolean 				autoLogin = false;
	
	private static final String TAG = Main.class.getSimpleName();
	public static Logger log = new Logger();
	
	public Main() {
		super();
		Random r = new Random();
	
		for(int i=0; i<(r.nextInt(10)+1); i++){
			TCPConnectionHandler temp = new TCPConnectionHandler("10.0.4.201");
			temp.start();
		}
	}
	
	public static void main(String[] args){
		new Main();
	}

}
