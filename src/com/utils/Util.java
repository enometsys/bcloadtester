package com.utils;

import com.Main;

public class Util {
	private static final String TAG = Util.class.getSimpleName();
	
	public static final String GAME_NAME = "Battle Clans";
	
	public static final int X_SIZE = 1280;
	public static final int Y_SIZE = 720;
	
	public static final int PREF_FPS = 60;
	
	public static final int STATE_MENU 	= 0;
	public static final int STATE_LOBBY = 1;
	public static final int STATE_ROOM 	= 2;
	public static final int STATE_GAME 	= 3;
	
	public static final int TCP_PORT		= 8020;	//MITM PORT: 8020; SERVER PORT: 5020
	
	public static String getStateName(int stateId){
		switch(stateId){
			case STATE_MENU:
				return "Menu State";
			case STATE_LOBBY:
				return "Lobby State";
			case STATE_ROOM:
				return "Room State";
			case STATE_GAME:
				return "Game State";
		}
		
		Main.log.e(TAG, "State name request with invalid state Id");
		return "NOT A VALID STATE ID!";
	}
}
