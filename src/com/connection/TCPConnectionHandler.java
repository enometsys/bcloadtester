package com.connection;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

import com.Main;
import com.packet.tcp.TCPPacket;
import com.packet.tcp.TCPPacket.Scope;
import com.packet.tcp.TCPPacketBuilder;
import com.utils.Util;

public class TCPConnectionHandler extends Thread{
	private static final String TAG = TCPConnectionHandler.class.getSimpleName();
	
	private Socket 				connectionSocket;
	
	private ObjectOutputStream 	out;
	
	private boolean				connected;
	
	public TCPConnectionHandler(String ipAddress) {
		
		try {
			Main.log.d(TAG, "TCP Connecting to " + ipAddress + ":" + Util.TCP_PORT);
			connectionSocket = new Socket(ipAddress, Util.TCP_PORT);
			
			out = new ObjectOutputStream(connectionSocket.getOutputStream());
			
		} catch (IOException e) {
			//e.printStackTrace();
			Main.log.e(TAG, "Failed to connect to server! " + e.getMessage());
		}
	}

	@Override
	public void run() {
		if (connectionSocket != null){	
			Random r = new Random();
			
			sendPacket(TCPPacketBuilder.getJoinServer("Random" + r.nextInt(99)));
			
			connected = true;
			while(isConnected()){
				switch(r.nextInt(10)){
					case 1: sendPacket(TCPPacketBuilder.getChat(Scope.GLOBAL, "Anonymous", "Random")); break;
					case 2: sendPacket(TCPPacketBuilder.getHostMatch("Random")); break;
					case 3: sendPacket(TCPPacketBuilder.getJoinMatch(r.nextInt(3))); break;
					default: sendPacket(TCPPacketBuilder.getRefreshRooms()); break;
				}
			}
		}
	}
	
	private synchronized boolean isConnected(){
		return connected;
	}

	public void sendPacket(TCPPacket outPacket) {
		Main.log.s(TAG, outPacket);
		if (connectionSocket != null){
			try {
				out.writeObject(outPacket);
			} catch (IOException e) {}
		}
	}
	
	public void disconnect() {
		synchronized(this){
			connected = false;
		}
		
		try {
			connectionSocket.close();
		} catch (IOException e) {}
	}
}
